<?php

namespace app\admin\model;

use think\Model;

class SysuserModel extends Model
{
    public function roles()
    {
        return $this->belongsToMany('SysroleModel','Sysrole_user','role_id','user_id');
    }

}