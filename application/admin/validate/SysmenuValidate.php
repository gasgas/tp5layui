<?php

namespace app\admin\validate;
;

use think\Validate;

class SysmenuValidate extends Validate
{
    protected $rule = [
        'name'  => 'require|max:255|',
        'name'  => 'unique:sysmenu',
        'age'   => 'number|between:1,120',
        'email' => 'email',
    ];

    protected $message = [
        'name.require' => '名称必须',
        'name.max'     => '名称最多不能超过25个字符',
        'name.unique'     => '权限规则重复',
        'age.number'   => '年龄必须是数字',
        'age.between'  => '年龄只能在1-120之间',
        'email'        => '邮箱格式错误',
    ];

}